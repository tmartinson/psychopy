import csv
from pygame import mixer
import time 
import os

# Load data from CSV file
data = []
with open('test_toe.csv', 'r', encoding="utf-8-sig") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        data.append(row)

mixer.init()

# Initialize PsychoPy window
#win = visual.Window(size=(800, 600), fullscr=False, color='black')

# Create text stimulus
# text_stim = visual.TextStim(win=win, text='', height=30, color='black')
#text_stim = visual.TextStim(win=win, name='test_word_hmm',
#        text='',
#        font='Open Sans',
#        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
#        color='white', colorSpace='rgb', opacity=None, 
#        languageStyle='LTR',
#        depth=0.0);
        
audio_file_path = 'first_6_01.ogg'
mixer.music.load(audio_file_path)
mixer.music.set_volume(1.0)
mixer.music.play()
#sound_stim = sound.Sound(audio_file_path, stereo=True, hamming=True)
#sound_stim.play()

time.sleep(float(data[0]['Start']))

# Main experiment loop
#for trial in data:
for index in range(0, len(data)):
    
    trial = data[index]
    stim_word = trial['Word']
    start_time = float(trial['Start'])
    end_time = float(trial['End'])
    
    # Display word from start_time to end_time
    #text_stim.text = stim_word
    print(stim_word)
    #text_stim.draw()
    #win.flip()
    time.sleep(end_time - start_time)
    #core.wait(start_time)  # Wait until start time
    #core.wait(end_time - start_time)  # Display word for the specified duration
    
    # Clear the screen
    #win.flip()
    #os.system('clear')
    #core.wait(0.5)  # Pause for 0.5 seconds between words (adjust as needed)
    if index != len(data):  
        time.sleep(float(data[index+1]['Start']) - end_time)
    
# Close PsychoPy window at the end of the experiment
#win.close()
